-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.20 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             11.2.0.6213
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table skatepro.customers
CREATE TABLE IF NOT EXISTS `customers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `role_email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table skatepro.customers: 2 rows
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
REPLACE INTO `customers` (`id`, `email`, `password`, `name`, `created_at`, `updated_at`) VALUES
	(1, 'ziabzu@gmail.com', '$2y$10$Tdvk/6vn/zN7r5fSjzHqAODgPBaUIbBsgUlwYQ2vaTKf6TgPGhaBC', 'Zia', '2021-03-31 20:36:32', '2021-01-21 11:05:44'),
	(2, 'konopelski.autumn@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'Myrtle Corwin', '2021-02-28 20:36:32', '2020-12-29 20:36:32');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

-- Dumping structure for table skatepro.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `amount` double(8,2) NOT NULL,
  `currency` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'dk',
  `comments` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_status` enum('pending','in-process','ready','cancelled','deleted','completed') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `status` enum('active','deleted') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table skatepro.orders: ~81 rows (approximately)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
REPLACE INTO `orders` (`id`, `user_id`, `amount`, `currency`, `comments`, `order_status`, `status`, `created_at`, `updated_at`) VALUES
	(33, 1, 2000.10, 'dk', 'comments sdfasdf', 'in-process', 'active', '2021-01-11 22:47:01', '2021-01-29 17:08:01'),
	(34, 1, 2000.10, 'dk', 'comments sdfasdf', 'in-process', 'active', '2021-01-11 22:48:23', '2021-03-07 19:41:53'),
	(35, 1, 160.00, 'dk', 'comments sdfasdf', 'in-process', 'active', '2021-01-07 13:12:56', '2021-01-29 17:07:58'),
	(36, 1, 160.00, 'dk', 'comments sdfasdf', 'pending', 'active', '2021-01-07 13:38:28', '2021-03-07 19:41:50'),
	(37, 1, 2020.20, 'dk', 'comments sdfasdf', 'cancelled', 'active', '2021-01-07 18:30:27', '2021-02-02 13:00:59'),
	(38, 1, 160.00, 'dk', 'comments sdfasdf', 'in-process', 'active', '2021-01-08 21:02:47', '2021-01-29 17:07:47'),
	(39, 1, 160.00, 'dk', 'comments sdfasdf', 'in-process', 'active', '2021-01-08 21:02:53', '2021-01-29 17:07:56'),
	(40, 1, 680.10, 'dk', 'comments sdfasdf', 'in-process', 'active', '2021-01-10 05:32:30', '2021-01-29 17:07:47'),
	(41, 1, 190.00, 'dk', 'comments sdfasdf', 'in-process', 'active', '2021-01-21 12:41:30', '2021-02-01 05:15:16'),
	(42, 1, 1830.00, 'dk', 'comments sdfasdf', 'in-process', 'active', '2021-01-21 16:25:46', '2021-02-01 05:14:39'),
	(43, 1, 2070.00, 'dk', 'comments sdfasdf', 'in-process', 'active', '2021-01-21 21:44:08', '2021-01-30 02:08:41'),
	(44, 1, 220.00, 'dk', 'comments', 'in-process', 'active', '2021-01-22 14:13:54', '2021-01-30 10:38:11'),
	(45, 1, 60.00, 'dk', 'comments', 'in-process', 'active', '2021-01-22 16:25:47', '2021-01-30 08:18:30'),
	(46, 1, 60.00, 'dk', 'comments', 'in-process', 'active', '2021-01-22 16:47:03', '2021-01-29 17:08:03'),
	(47, 1, 60.00, 'dk', 'comments', 'in-process', 'active', '2021-01-22 16:48:26', '2021-01-29 17:07:47'),
	(48, 1, 60.00, 'dk', 'comments', 'in-process', 'active', '2021-01-22 16:48:53', '2021-01-29 17:07:47'),
	(49, 1, 60.00, 'dk', 'comments', 'in-process', 'active', '2021-01-22 16:50:42', '2021-01-29 17:07:47'),
	(50, 1, 740.00, 'dk', 'comments', 'cancelled', 'active', '2021-01-22 17:19:20', '2021-01-29 17:07:47'),
	(51, 1, 320.00, 'dk', 'comments', 'in-process', 'active', '2021-01-26 14:35:14', '2021-01-29 17:07:47'),
	(52, 1, 120.00, 'dk', 'comments', 'in-process', 'active', '2021-01-29 17:11:02', '2021-01-30 02:08:18'),
	(53, 1, 120.00, 'dk', 'comments', 'cancelled', 'active', '2021-01-29 17:14:27', '2021-03-07 19:42:26'),
	(54, 1, 240.00, 'dk', 'comments', 'completed', 'active', '2021-03-28 17:33:01', '2021-03-07 17:45:10'),
	(55, 1, 50.00, 'dk', 'comments', 'completed', 'active', '2021-05-28 19:43:51', '2021-03-07 21:55:47'),
	(56, 1, 80.00, 'dk', 'comments', 'completed', 'active', '2021-01-30 02:16:34', '2021-01-30 08:18:34'),
	(57, 1, 840.10, 'dk', 'comments', 'in-process', 'active', '2021-01-30 11:28:45', '2021-01-30 19:38:57'),
	(59, 1, 80.00, 'dk', 'comments', 'in-process', 'active', '2021-01-31 11:41:09', '2021-02-01 05:13:56'),
	(60, 1, 160.00, 'dk', 'comments', 'in-process', 'active', '2021-01-31 11:50:46', '2021-02-01 05:13:48'),
	(61, 1, 550.40, 'dk', '', 'pending', 'active', '2021-02-01 17:27:50', '2021-02-01 17:27:50'),
	(62, 1, 30.00, 'dk', 'comments', 'pending', 'active', '2021-02-01 18:05:15', '2021-03-07 21:55:21'),
	(63, 1, 26.00, 'dk', 'comments', 'pending', 'active', '2021-02-01 18:05:21', '2021-03-07 21:55:18'),
	(64, 1, 26.00, 'dk', 'comments', 'pending', 'active', '2021-02-01 18:14:40', '2021-03-07 21:55:15'),
	(66, 1, 2680.20, 'dk', 'comments', 'in-process', 'active', '2021-02-01 18:14:46', '2021-02-02 01:54:21'),
	(68, 1, 2680.20, 'dk', 'comments', 'in-process', 'active', '2021-02-01 18:20:06', '2021-02-02 01:53:35'),
	(69, 1, 2680.20, 'dk', 'comments', 'in-process', 'active', '2021-02-01 18:21:34', '2021-02-02 00:08:49'),
	(70, 1, 80.00, 'dk', 'comments', 'completed', 'active', '2021-02-01 18:24:56', '2021-02-02 01:55:17'),
	(76, 1, 110.00, 'dk', 'comments', 'pending', 'active', '2021-02-02 11:27:19', '2021-02-02 11:27:19'),
	(77, 1, 110.00, 'dk', 'comments', 'pending', 'active', '2021-02-02 11:27:25', '2021-02-02 11:27:25'),
	(78, 1, 240.00, 'dk', 'comments', 'pending', 'active', '2021-02-02 11:27:56', '2021-02-02 11:27:56'),
	(79, 1, 240.00, 'dk', 'comments', 'pending', 'active', '2021-02-02 11:34:40', '2021-02-02 11:34:40'),
	(80, 1, 110.00, 'dk', 'comments', 'pending', 'active', '2021-02-02 11:35:01', '2021-02-02 11:35:01'),
	(81, 1, 110.00, 'dk', 'comments', 'pending', 'active', '2021-02-02 11:37:08', '2021-02-02 11:37:08'),
	(83, 1, 160.00, 'dk', 'comments', 'pending', 'active', '2021-02-02 17:42:11', '2021-02-02 17:42:11'),
	(84, 1, 3280.00, 'dk', 'comments', 'pending', 'active', '2021-02-02 18:08:38', '2021-02-02 18:08:38'),
	(85, 1, 3280.00, 'dk', 'comments', 'pending', 'active', '2021-02-02 18:11:15', '2021-02-02 18:11:15'),
	(87, 1, 37.00, 'dk', 'comments', 'completed', 'active', '2021-02-03 15:19:37', '2021-03-07 21:57:35'),
	(88, 1, 240.00, 'dk', 'comments', 'pending', 'active', '2021-02-04 19:01:10', '2021-02-04 19:01:10'),
	(89, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 11:52:11', '2021-02-11 11:52:11'),
	(90, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 11:52:24', '2021-02-11 11:52:24'),
	(91, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 11:52:27', '2021-02-11 11:52:27'),
	(92, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 11:52:40', '2021-02-11 11:52:40'),
	(93, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 11:52:41', '2021-02-11 11:52:41'),
	(94, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 11:52:42', '2021-02-11 11:52:42'),
	(95, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 11:52:48', '2021-02-11 11:52:48'),
	(96, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 11:52:56', '2021-02-11 11:52:56'),
	(97, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 11:53:11', '2021-02-11 11:53:11'),
	(98, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 11:53:16', '2021-02-11 11:53:16'),
	(99, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:19:15', '2021-02-11 12:19:15'),
	(100, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:23:36', '2021-02-11 12:23:36'),
	(101, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:23:57', '2021-02-11 12:23:57'),
	(102, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:25:52', '2021-02-11 12:25:52'),
	(103, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:28:43', '2021-02-11 12:28:43'),
	(104, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:29:25', '2021-02-11 12:29:25'),
	(105, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:30:05', '2021-02-11 12:30:05'),
	(106, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:31:12', '2021-02-11 12:31:12'),
	(107, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:37:44', '2021-02-11 12:37:44'),
	(108, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:38:10', '2021-02-11 12:38:10'),
	(109, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:39:02', '2021-02-11 12:39:02'),
	(110, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:42:41', '2021-02-11 12:42:41'),
	(111, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:43:34', '2021-02-11 12:43:34'),
	(112, 11, 550.40, 'dk', '', 'pending', 'active', '2021-02-11 12:44:09', '2021-02-11 12:44:09'),
	(113, 11, 550.40, 'dk', '', 'ready', 'active', '2021-02-11 13:25:20', '2021-02-11 13:28:19');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Dumping structure for table skatepro.order_items
CREATE TABLE IF NOT EXISTS `order_items` (
  `id` int unsigned NOT NULL DEFAULT '0',
  `order_id` int unsigned NOT NULL DEFAULT '0',
  `product_id` int unsigned NOT NULL DEFAULT '0',
  `ean` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `quantity` smallint unsigned NOT NULL DEFAULT '0',
  `price` double(8,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table skatepro.order_items: ~0 rows (approximately)
/*!40000 ALTER TABLE `order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_items` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
