<div class="col-lg-4 col-md-4 mt-1">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <i class="bi bi-people card-icon"></i>
                </div>
                <div class="col-8">
                    <div class="">
                        <div class=""><span class="count"><?php echo $customer_numbers; ?></span></div>
                        <div class="">Customers</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>