<?php

require_once("vendour/Carbon/autoload.php");

use Carbon\Carbon;

function chartLabels($year): array 
{

    // zeroFill chartData
    $year = $year ?? Carbon::now()->format('Y');
    $carbonObject = Carbon::createFromDate($year, 1, 1);

    $carbonObject = $carbonObject->startOfYear();

    $formatted = [];

    for ($i = 0; $i<12; $i++) {
        
        $formatted[] = $carbonObject->format('Y-M');
        $carbonObject = $carbonObject->addMonth();    

    }

    return $formatted;

}