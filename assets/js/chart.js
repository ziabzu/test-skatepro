$(function() {

  fetchChartData(2021);

});


function fetchChartData(year) {

  $.ajax({
    type: 'get',
    url: _base_url + 'ajax-chart.php?year=' + year,
    dataType: 'json',
    success: function (data, status, xhr) {// success callback function

      console.log('data: ', data);
      console.log('data: ', data['customers']);
      console.log('data: ', data['orders']);
      console.log('data order-chart-data: ', data['order-chart-data']);

      drawChart(data);

    }

  })

}

function drawChart(data) {

  Highcharts.chart('div-chart', {
      chart: {
        type: 'spline'
      },
      title: {
        text: 'Monthly Customers and Orders'
      },
      subtitle: {
        text: 'Source: Tried by Zia'
      },
      xAxis: {
        categories:  data['labels']['x']
      },
      yAxis: {
        title: {
          text: 'Numbers'
        },
        // labels: {
        //   formatter: function () {
        //     return this.value + '°';
        //   }
        // }
      },
      tooltip: {
        crosshairs: true,
        shared: true
      },
      plotOptions: {
        spline: {
          marker: {
            radius: 4,
            lineColor: '#666666',
            lineWidth: 1
          }
        }
      },
      series: [{
        name: 'Customers',
        marker: {
          symbol: 'circle'
        },
        data: data['customers']
    
      }, {
        name: 'Orders',
        marker: {
          symbol: 'circle'
        },
        data: data['orders']
      }]
    });
}