    <!-- Add JS in end of body to maitain rendering speed   -->
    <script type="text/javascript">
        var _base_url = "<?php echo $base_url; ?>";
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>    
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>    
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/9.0.1/highcharts.min.js" integrity="sha512-EIJ4kNprq9ImB7okK8Wy5o4frtpx1zERP1A3ZrLXr5nOGEWhVViC1XVuzJ8GGG89sJ/bKbtw+6TsQlul9/WcEg==" crossorigin="anonymous"></script>

    <script src="<?php echo $base_url ?>assets/js/chart.js"></script>

</body>
</html>