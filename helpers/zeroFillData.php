<?php

require_once("vendour/Carbon/autoload.php");

use Carbon\Carbon;

function zeroFillChartData($chartData, $year): array 
{

    // zeroFill chartData
    $year = $year ?? Carbon::now()->format('Y');
    $carbonObject = Carbon::createFromDate($year, 1, 1);

    $carbonObject = $carbonObject->startOfYear();

    $formatted = [];

    for ($i = 0; $i<12; $i++) {
        
        $found = 0;

        foreach($chartData as $orderChartObj) {
            if($orderChartObj->monthYear == $carbonObject->format('Y-M')) $found = $orderChartObj->total;
        }

        $formatted[] = intval($found);
        
        $carbonObject = $carbonObject->addMonth();    

    }

    return $formatted;

}