<?php

include_once("models/Customer.php");

class CustomerController
{
    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function fetchChartData($year = null)
    {

        $customer = new Customer($this->db);
        return $customer->fetchChartData($year);

    }
    
}
