<?php

include_once("models/Order.php");

class OrderController
{
    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function fetchChartData($year = null)
    {

        $order = new Order($this->db);
        return $order->fetchChartData($year);

    }

    
}
