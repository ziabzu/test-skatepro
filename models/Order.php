<?php

require_once('models/IModel.php');

use Carbon\Carbon;

class Order implements IModel
{

    private $db;

    // Here will be more attributes with passage of time


    public function __construct($db)
    {
        $this->db = $db;
    }

    public function fetchChartData($year = null)
    {
        $year = $year ?? Carbon::now()->format('Y');

        $query = "SELECT DATE_FORMAT(o.created_at,'%Y-%b') AS `monthYear`, SUM(o.amount) AS `total` 
            FROM `orders` o 
            WHERE 
                o.order_status =  :order_status 
                AND DATE_FORMAT(o.created_at,'%Y') = :year 
            GROUP BY DATE_FORMAT(o.created_at,'%Y-%b')
            ORDER BY DATE_FORMAT(o.created_at,'%Y-%b');";
       

        $stmt = $this->db->prepare($query);
        $stmt->execute(array(':year' => $year, ':order_status' => 'completed'));
        
        $obj = $stmt->fetchAll(PDO::FETCH_OBJ);
        $stmt = null;

        return ($obj);

    }

    public function fetchAll($month = null, $year = null)
    {

		$from = $from ?? Carbon::now()->startOfMonth();
        $to = $to ?? Carbon::now()->endOfMonth();
        
        $query = "
            SELECT count(*) AS `total` FROM `orders` o 
            WHERE 
            	o.`created_at` BETWEEN :from AND :to
            LIMIT 1
        ";

        $stmt = $this->db->prepare($query);
        $stmt->execute(array(':from' => $from->toDateTimeString(), ':to' => $to->toDateTimeString()));
        $obj = $stmt->fetch(PDO::FETCH_OBJ);
        $stmt = null;

        if (!$obj) return ('Error');

        return ($obj->total);
    }
}
