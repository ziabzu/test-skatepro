<?php

require_once('models/IModel.php');

use Carbon\Carbon;

class Customer implements IModel
{

    private $db;

    // Here will be more attributes with passage of time

    // private $id;
    // private $email;
    // private $name;
    // private $password;
    // etc

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function fetchChartData($year = null)
    {
        $year = $year ?? Carbon::now()->format('Y');

        $query = "SELECT DATE_FORMAT(c.created_at,'%Y-%b') AS `monthYear`, COUNT(*) AS `total` 
            FROM `customers` c
            WHERE 
                DATE_FORMAT(c.created_at,'%Y') = :year 
            GROUP BY DATE_FORMAT(c.created_at,'%Y-%b')
            ORDER BY DATE_FORMAT(c.created_at,'%Y-%b');";
       

        $stmt = $this->db->prepare($query);
        $stmt->execute(array(':year' => $year));
        
        $obj = $stmt->fetchAll(PDO::FETCH_OBJ);
        $stmt = null;

        return ($obj);

    }

    /**
     * fetchAll
     * @param $from Carbon object
     * @param $to Carbon object
     * fetch Customers 
     */
    public function fetchAll($from = null, $to = null)
    {

        $from = $from ?? Carbon::now()->startOfMonth();
        $to = $to ?? Carbon::now()->endOfMonth();
        
        $query = "
            SELECT count(*) AS `total` FROM `customers` c 
            WHERE 
            	c.`created_at` BETWEEN :from AND :to
            LIMIT 1
        ";

        $stmt = $this->db->prepare($query);
        $stmt->execute(array(':from' => $from->toDateTimeString(), ':to' => $to->toDateTimeString()));
        $obj = $stmt->fetch(PDO::FETCH_OBJ);
        $stmt = null;

        if (!$obj) return ('Error');

        return ($obj->total);
    }

   
}
