
<?php 
    // Install carbon
    require_once("vendour/Carbon/autoload.php");
    require_once("helpers/zeroFillData.php");
    require_once("helpers/chartLabels.php");
    require_once("db/DB.php");
    include_once("controllers/OrderController.php");
    include_once("controllers/CustomerController.php");
?>

<?php

$DB = new DB();

// Getting Order Chart Data
$orderController = new OrderController($DB->dbh);
$formattedOrderData = zeroFillChartData($orderController->fetchChartData($_GET['year']), $_GET['year']);

// Getting Customer chart data
$cutomserController = new CustomerController($DB->dbh);
$formattedCustomerChartData = zeroFillChartData($cutomserController->fetchChartData($_GET['year']), $_GET['year']);

$labels['x'] = chartLabels($_GET['year']);

echo json_encode(array(
    'labels' => $labels,
    'orders' => $formattedOrderData,
    'customers' => $formattedCustomerChartData, 
));
