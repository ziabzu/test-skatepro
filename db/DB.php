<?php

// Create a new connection.
// You'll probably want to replace hostname with localhost in the first parameter.
// Note how we declare the charset to be utf8mb4.  This alerts the connection that we'll be passing UTF-8 data.  This may not be required depending on your configuration, but it'll save you headaches down the road if you're trying to store Unicode strings in your database.  See "Gotchas".
// The PDO options we pass do the following:
// PDO::ATTR_ERRMODE enables exceptions for errors.  This is optional but can be handy.
// PDO::ATTR_PERSISTENT disables persistent connections, which can cause concurrency issues in certain cases.  See "Gotchas".

class DB
{

    // db settings
    private $host   = 'localhost';
    private $user   = 'root';
    private $dbname = 'skatepro';
    private $pass   = 'test123';

    public $dbh = null;
    private $error;

    public function __construct()
    {

        // if(!is_null($this->dbh)) {

            // Set DSN
            $dsn = 'mysql: host=' . $this->host . ';dbname=' . $this->dbname;
            // Set options
            $options = array(
                PDO::ATTR_PERSISTENT            => true,
                PDO::ATTR_ERRMODE               => PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_INIT_COMMAND    => 'SET NAMES UTF8'
            );
            // Create a new PDO instanace
            try {

                $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
                return $this->dbh;
            
            }
            // Catch any errors
            catch (PDOException $e) {
                $this->error = $e->getMessage();
                echo $this->error;
                exit;
            }

        // }

    }

}
