<?php

interface IModel
{

    public function fetchAll($from, $to);
    
    // Here will come more abstract functions
    // public function fetch($id);
    // public function add(Entity $entity);
    // public function update(Entity $entity);
    // public function remove(Entity $entity);

}