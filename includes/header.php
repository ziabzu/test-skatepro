<?php

require_once("includes/all-lib.php");

$DB = new DB();
$controller = new DashBoardController($DB->dbh);

?>
<!doctype html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Skate Pro Admin Template</title>
    <meta name="description" content="Ela Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favourite icons  -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $base_url ?>assets/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo $base_url ?>assets/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo $base_url ?>assets/favicons/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $base_url ?>assets/favicons/site.webmanifest">
    <link rel="mask-icon" href="<?php echo $base_url ?>assets/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <link rel="shortcut icon" href="<?php echo $base_url ?>assets/favicons/favicon.ico">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-config" content="<?php echo $base_url ?>assets/favicons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

    <link href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" rel="stylesheet" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highcharts/9.0.1/css/highcharts.min.css" integrity="sha512-hGGTPscQ5tEG/4LWx4oXiF2SWumn6Q3sA8MlZQYynN1Qs1Gvm/8rI4j+Cq8oTUsLcsf+ovh0Nuap5HzZjYv92Q==" crossorigin="anonymous" />

    <link rel="stylesheet" href="<?php echo $base_url ?>assets/css/style.css">

</head>

    <body>