<?php

include_once("models/Customer.php");
include_once("models/Order.php");
include_once("models/Revenue.php");

class DashBoardController
{
    private $db = null;

    public function __construct($db)
    {
        $this->db = $db;
    }

    public function fetchCustomers($from = null, $to = null)
    {

        $customer = new Customer($this->db);
        $customer_numbers = $customer->fetchAll();
        include 'views/partials/customers.php';

    }

    public function fetchOrders($from, $to)
    {

        $order = new Order($this->db);
        $order_numbers = $order->fetchAll($from, $to);
        include 'views/partials/orders.php';

    }

    public function fetchRevenue($from, $to)
    {

        $revenue = new Revenue($this->db);
        $revenue = $revenue->fetchAll($from, $to);
        include 'views/partials/revenue.php';

    }

   
    
}
