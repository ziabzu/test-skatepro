<?php

require_once('models/IModel.php');

use Carbon\Carbon;

class Revenue implements IModel
{

    private $db;
    // Here will be more attributes with passage of time


    public function __construct($db)
    {
        $this->db = $db;
    }

    public function fetchAll($month = null, $year = null)
    {

        $from = $from ?? Carbon::now()->startOfMonth();
        $to = $to ?? Carbon::now()->endOfMonth();
        
        $query = "
            SELECT count(*) AS `total` FROM `orders` o 
            WHERE TRUE
                AND `o`.`order_status` = :order_status
            	AND o.`created_at` BETWEEN :from AND :to
            LIMIT 1
        ";

        $stmt = $this->db->prepare($query);
        $stmt->execute(array(
                ':from' => $from->toDateTimeString(), 
                ':to' => $to->toDateTimeString(),
                ':order_status' => 'completed'
                )
            );
        $obj = $stmt->fetch(PDO::FETCH_OBJ);
        $stmt = null;

        if (!$obj) return ('Error');

        return ($obj->total);
    }
}
