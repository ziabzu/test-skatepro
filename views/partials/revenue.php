<div class="col-lg-4 col-md-4 mt-1">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-4">
                    <i class="bi bi-cash-stack card-icon"></i>
                </div>
                <div class="col-8">
                    <div class="">
                        <div><span class="count"><?php echo $revenue; ?></span></div>
                        <div>Revenue</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>