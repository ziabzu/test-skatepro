

<!-- // Clear cache -->
<?php
    header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
?>

<?php include('includes/functions.php') ?>
<?php include('includes/header.php') ?>

<div class="container">
    <!-- ======= Top Bar ======= -->
    <?php include('includes/topbar.php') ?>
    <!-- ======= END Top Bar ======= -->

    <div class="bg-light">

        <!-- Cards -->
        <?php include('includes/cards.php') ?>
        <!-- End Cards -->

    </div>

    <div class="bg-light mt-5">
        <div class="container mt-3">
            <select id="select-chart-year" onchange="fetchChartData($('#select-chart-year').val());" class="form-select" aria-label="Default select example">
                <option selected>Year</option>
                <option value="2021">2021</option>
                <option value="2020">2020</option>
                <option value="2019">2019</option>
            </select>
            <div class="card">
                <div id="div-chart"></div>
            </div>
        </div>
    </div>

</div>


<?php require('includes/footer.php') ?>